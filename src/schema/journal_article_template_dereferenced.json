{
    "title": "Article deposit",
    "type": "object",
    "properties": {
      "journal": {
        "title": "Journal Information",
        "type": "object",
        "properties": {
          "full_title": {
            "title": "Journal title",
            "type": "string",
            "examples": [
              "Journal of Psychoceramics"
            ]
          },
          "abbrev_title": {
            "title": "Abbreviated journal title",
            "type": "string",
            "examples": [
              "JOPC"
            ]
          },
          "volume": {
            "title": "Volume",
            "type": "number"
          },
          "issue": {
            "title": "Issue",
            "type": "number"
          },
          "publication_date": {
            "title": "Publication date",
            "type": "string",
            "format": "date"
          },
          "issns": {
            "title": "ISSNs",
            "description": "online and print ISSNs",
            "type": "array",
            "minItems": 1,
            "maxItems": 2,
            "items": {
              "type": "object",
              "anyOf": [
                {
                  "title": "ISSN",
                  "properties": {
                    "mediaType": {
                      "type": "string",
                      "const": "issn",
                      "title": "ISSN"
                    },
                    "issn": {
                      "type": "string",
                      "pattern": "[\\S]{4}\\-[\\S]{4}",
                      "title": "ISSN for print version"
                    }
                  }
                },
                {
                  "title": "eISSN",
                  "properties": {
                    "mediaType": {
                      "type": "string",
                      "const": "eissn",
                      "title": "eISSN"
                    },
                    "issn": {
                      "type": "string",
                      "pattern": "[\\S]{4}\\-[\\S]{4}",
                      "title": "ISSN for online version"
                    }
                  }
                }
              ]
            }
          }
        }
      },
      "depositor": {
        "title": "Depositor Information",
        "description": "This information allows us to update you on the progress of the deposit",
        "type": "object",
        "properties": {
          "depositor_name": {
            "title": "Depositor name",
            "description": "your name",
            "type": "string"
          },
          "email_address": {
            "type": "string",
            "title": "Email address",
            "description": "email address to send errors to",
            "format": "email"
          }
        }
      },
      "articles": {
        "title": "Articles",
        "type": "array",
        "minItems": 1,
        "uniqueItems": true,
        "items": {
          "title": "Journal Article",
          "type": "object",
          "properties": {
            "titles": {
              "title": "Titles",
              "type": "array",
              "minItems": 1,
              "items": {
                "title": "title",
                "type": "object",
                "properties": {
                  "title_language": {
                    "title": "Title language",
                    "description": "The ISO code for the language the title text is in",
                    "type": "string",
                    "default": "en",
                    "enum": [
                      "en",
                      "es",
                      "pt"
                    ]
                  },
                  "title_text": {
                    "title": "Title text",
                    "description": "The title of the article in the language selected",
                    "type": "string"
                  }
                }
              }
            },
            "resolution_information": {
              "title": "Resolution Information",
              "type": "object",
              "properties": {
                "doi": {
                  "title": "DOI",
                  "description": "The DOI you want to assign to this article",
                  "type": "string",
                  "pattern": "^10\\.\\d{4,9}/[-._;()/:A-Z0-9^\\s]+",
                  "message": "You must enter a valid DOI"
                },
                "resource": {
                  "title": "Resolution URL",
                  "description": "URL the DOI should resolve to",
                  "type": "string",
                  "format": "url"
                },
                "similarity_check_url": {
                  "title": "Similarity Check URL",
                  "description": "URL for Similarity Check to get full text",
                  "type": "string",
                  "format": "url"
                }
              }
            },
            "publication_date": {
              "title": "Publication date",
              "type": "string",
              "format": "date"
            },
            "article_location": {
              "title": "Article location",
              "description": "enter either the page-span or the article number",
              "type": "array",
              "minItems": 1,
              "maxItems": 1,
              "items": {
                "oneOf": [
                  {
                    "title": "Page span",
                    "type": "object",
                    "additionalProperties": false,
                    "properties": {
                      "first_page": {
                        "title": "First page",
                        "type": "number"
                      },
                      "last_page": {
                        "title": "Last page",
                        "type": "number"
                      }
                    },
                    "required": [
                      "first_page",
                      "last_page"
                    ]
                  },
                  {
                    "title": "Article number",
                    "type": "object",
                    "additionalProperties": false,
                    "properties": {
                      "number": {
                        "title": "Number",
                        "type": "number"
                      }
                    },
                    "required": [
                      "number"
                    ]
                  }
                ]
              }
            },
            "abstract": {
              "title": "Abstract",
              "description": "enter a JATs abstract",
              "type": "string"
            },
            "contributors": {
              "title": "Contributors",
              "type": "object",
              "properties": {
                "people": {
                  "title": "People",
                  "type": "array",
                  "items": {
                    "title": "Person",
                    "type": "object",
                    "properties": {
                      "-contributor_role": {
                        "title": "Contributor role",
                        "type": "string",
                        "enum": [
                          "author",
                          "editor",
                          "chair",
                          "translator"
                        ],
                        "default": "author"
                      },
                      "-sequence": {
                        "title": "Contributor sequence",
                        "type": "string",
                        "enum": [
                          "first",
                          "addtional"
                        ]
                      },
                      "ORCID": {
                        "type": "string",
                        "title": "ORCID",
                        "pattern": "(\\d{4}\\-\\d{4}\\-\\d{4}\\-\\d{3}(?:\\d|X))"
                      },
                      "given_name": {
                        "title": "Given name",
                        "type": "string"
                      },
                      "surname": {
                        "title": "Surname",
                        "type": "string"
                      },
                      "suffix": {
                        "title": "Suffix",
                        "description": "",
                        "type": "string"
                      }
                    }
                  }
                }
              }
            },
            "license": {
              "title": "License information",
              "description": "License information for AM, VOR",
              "type": "array",
              "minItems": 0,
              "maxItems": 10,
              "items": {
                "anyOf": [
                  {
                    "title": "Creative Common License",
                    "type": "object",
                    "additionalProperties": false,
                    "properties": {
                      "cc_url": {
                        "title": "Creative Commons license url",
                        "description": "The URL of the Creative Commons license",
                        "type": "string",
                        "enum": [
                          "https://creativecommons.org/licenses/by/4.0/",
                          "https://creativecommons.org/licenses/by-sa/4.0/",
                          "https://creativecommons.org/licenses/by-nd/4.0/",
                          "https://creativecommons.org/licenses/by-nc/4.0/",
                          "https://creativecommons.org/licenses/by-nc-sa/4.0/",
                          "https://creativecommons.org/licenses/by-nc-nd/4.0/"
                        ],
                        "defualt": "https://creativecommons.org/licenses/by/4.0/"
                      },
                      "qualifiers": {
                        "title": "",
                        "type": "object",
                        "additionalProperties": false,
                        "properties": {
                          "start_date": {
                            "title": "Start date",
                            "description": "The date at which the license takes effect (usually the same as the publication date)",
                            "type": "string",
                            "format": "date"
                          },
                          "jav": {
                            "title": "JAV version",
                            "type": "string",
                            "enum": [
                              "Accepted Manuscript",
                              "Version of Record"
                            ],
                            "default": "Version of Record"
                          }
                        },
                        "required": [
                          "start_date",
                          "jav"
                        ]
                      }
                    },
                    "required": [
                      "cc_url"
                    ]
                  },
                  {
                    "title": "Other License",
                    "type": "object",
                    "additionalProperties": false,
                    "properties": {
                      "other_url": {
                        "title": "License url",
                        "description": "A URL that points to a copyright statement or terms & conditions",
                        "type": "string",
                        "format": "uri",
                        "default": "https://example.com/license"
                      },
                      "qualifiers": {
                        "title": "",
                        "type": "object",
                        "additionalProperties": false,
                        "properties": {
                          "start_date": {
                            "title": "Start date",
                            "description": "The date at which the license takes effect (usually the same as the publication date)",
                            "type": "string",
                            "format": "date"
                          },
                          "jav": {
                            "title": "JAV version",
                            "type": "string",
                            "enum": [
                              "Accepted Manuscript",
                              "Version of Record"
                            ],
                            "default": "Version of Record"
                          }
                        },
                        "required": [
                          "start_date",
                          "jav"
                        ]
                      }
                    },
                    "required": [
                      "other_url"
                    ]
                  }
                ]
              }
            },
            "references": {
              "title": "References",
              "type": "array",
              "minLength": 1,
              "uniqueItems": true,
              "items": {
                "anyOf": [
                  {
                    "title": "DOI Only Reference",
                    "type": "object",
                    "additionalProperties": false,
                    "properties": {
                      "DOI": {
                        "type": "string",
                        "pattern": "^10\\.\\d{4,9}/[-._;()/:A-Z0-9^\\s]+"
                      }
                    },
                    "required": [
                      "DOI"
                    ]
                  },
                  {
                    "title": "Unstructured reference",
                    "type": "object",
                    "additionalProperties": false,
                    "properties": {
                      "reference": {
                        "type": "string"
                      }
                    },
                    "required": [
                      "reference"
                    ]
                  },
                  {
                    "title": "Structured reference",
                    "type": "object",
                    "additionalProperties": false,
                    "properties": {
                      "journal_title": {
                        "type": "string"
                      },
                      "author": {
                        "type": "string"
                      },
                      "first_page": {
                        "type": "number"
                      },
                      "year": {
                        "type": "string",
                        "pattern": "^[1-9][0-9][0-9][0-9]$"
                      }
                    },
                    "required": [
                      "journal_title",
                      "author",
                      "year"
                    ]
                  }
                ]
              }
            }
          }
        }
      }
    }
  }
